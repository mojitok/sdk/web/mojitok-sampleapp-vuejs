# Mojitok-SampleApp-VueJs

## 1. Quick Start with SampleApp Code

#### Step1. Clone the SampleApp Code

```
git clone https://gitlab.com/mojitok/mojitok-cloud/sdk/web/mojitok-sampleapp-vuejs.git
```

#### Step2. Download and install NodeJS if your system doesn't have it yet.
- npm version: 6 or higher
- node version: v12 or higher
#### Step3. Open a terminal and move to the project path.

```
cd mojitok-sampleapp-vuejs
```

#### Step4. Create a .npmrc file in the sample app and write it like the contents of the .npmrc.example file to access the UIKit installation.

Enter the issued token for the npm module in [AuthToken](https://gitlab.com/mojitok/mojitok-cloud/sdk/web/mojitok-sampleapp-vuejs/-/blob/master/.npmrc.example#L1-3).

#### Step5. Install packages which are used in the sample app.

```
npm install
```

#### Step6. Initialize parameters

```
Mojitok.setup(applicationID: "ApplicationID", userID: "userId", ..)
Mojitok.connect(applicationToken: "ApplicationToken", ..)
```

Enter the issued [ApplicationId](https://gitlab.com/mojitok/mojitok-cloud/sdk/web/mojitok-sampleapp-vuejs/-/blob/master/src/App.vue#L56) and [ApplicationToken](https://gitlab.com/mojitok/mojitok-cloud/sdk/web/mojitok-sampleapp-vuejs/-/blob/master/src/App.vue#L56).
The [userId](https://gitlab.com/mojitok/mojitok-cloud/sdk/web/mojitok-sampleapp-vuejs/-/blob/master/src/App.vue#L56)(shorten for userIdentifierOfYourApp) field is an identifier that identifies individual end users of your app, and you can enter an arbitrary value when testing.

#### Step7. Run
```
npm run serve
```

That's it!